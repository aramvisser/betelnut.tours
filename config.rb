# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Layouts
# https://middlemanapp.com/basics/layouts/
set :layout, false

set :google_analytics, ENV['GA'] if ENV['GA']

proxy "_redirects", "netlify_redirects", ignore: true
proxy "_headers", "netlify_headers", ignore: true

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

data.faq.each do |id, faq|
  proxy "faq/#{id}.html", 'faq_template.html', locals: { faq: faq }, ignore: true
end

helpers do
  def inline_svg(name)
    root = Middleman::Application.root
    file_path = "#{root}/source/images/#{name}.svg"
    if File.exists?(file_path)
      File.read(file_path)
    else
      "<span class='icon-error'>Icon #{name} not found</span>"
    end
  end
end

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end
