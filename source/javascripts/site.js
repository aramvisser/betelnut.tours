// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};


var welcome = document.getElementById('intro');
var menu = document.getElementById('nav');

function parallax() {
  var yPos = window.pageYOffset / 2;
  var coords = '0% '+ yPos + 'px';
  welcome.style.backgroundPosition = coords;
}

function menuClass() {
  var yPos = window.pageYOffset;

  if(yPos < 200) {
    menu.classList.add('is-transparent');
  } else {
    menu.classList.remove('is-transparent');
  }
}

//window.addEventListener("scroll", parallax, { passive: true });
window.addEventListener("scroll", debounce(menuClass), { passive: true });
menuClass();

document.querySelector('[data-nav-toggle]').addEventListener('click', function(e) {
  e.preventDefault();
  document.body.classList.toggle('has-nav-visible');
});


function initModal() {
  let modalContent = document.querySelector('[data-overlay-content]');
  let modalTitle = document.querySelector('[data-overlay-title]');

  for (let closer of document.querySelectorAll('[data-overlay-close]')) {
    closer.addEventListener('click', function(e) {
      if('overlayClose' in e.target.dataset) {
        e.preventDefault();
        document.body.classList.remove('has-overlay-visible');
      }
    });
  }


  for (let element of document.querySelectorAll('[data-modal-content]')) {
    element.addEventListener('click', function(e) {
      e.preventDefault();

      let content = decodeURI(element.dataset.modalContent);
      let title = element.dataset.modalTitle;

      modalContent.innerHTML = content;
      modalTitle.innerHTML = title;
      document.body.classList.add('has-overlay-visible');
    });
  }
}

initModal();


document.querySelector('[data-nav-close-click]').addEventListener('click', function(e) {
  document.body.classList.remove('has-nav-visible');
}, { passive: true });


var nav_sections = new Map();
var nav_items = document.getElementById('nav').getElementsByTagName('a');

function initNavSections() {
  nav_sections.clear();

  for(let i = nav_items.length - 1; i >= 0; i--) {
    let link = nav_items[i];

    if(link.hash != '') {
      let target = document.querySelector(link.hash);
      
      if(target) {
        var offset = target.offsetTop;
        offset -= window.innerHeight * 0.25;
        nav_sections.set(offset, link);
      }
    }
  }
}

var current_active_nav = null;

function findActiveNavItem() {
  var yPos = window.pageYOffset;

  if((window.innerHeight * 1.25 + yPos) >= document.body.offsetHeight) {
    setActiveNavItem(nav_sections.values().next().value);
    return;
  }

  for (var [offset, link] of nav_sections.entries()) {
    if(yPos >= offset) {
      setActiveNavItem(link);
      break;
    }
  }
}

function setActiveNavItem(item) {
  if(item != current_active_nav) {
    if (current_active_nav) { current_active_nav.classList.remove('is-current') }
    item.classList.add('is-current');
    current_active_nav = item;
  }
}

window.addEventListener('load', initNavSections);
window.addEventListener('resize', initNavSections);
window.addEventListener("scroll", findActiveNavItem, { passive: true });

initNavSections();
findActiveNavItem();
